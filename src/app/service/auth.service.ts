import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiurl = 'http://localhost/jmserviceprintbackend/api/users';

  constructor(private http: HttpClient) {}
  onLogin(usercred: any) {
    return this.http.post(this.apiurl, usercred);
  }
  IsloggedIn() {
    return localStorage.getItem('token') != null;
  }
  GetToken() {
    return localStorage.getItem('token') || '';
  }
}
