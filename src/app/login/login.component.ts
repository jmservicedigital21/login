import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  messageClass = '';
  message = '';
  customerid: any;
  editdata: any;
  reponsedata: any;

  constructor(private service: AuthService, private route: Router) {
    localStorage.clear();
  }

  login = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  ngOnInit(): void {}

  onLogin() {
    if (this.login.valid) {
      this.service.onLogin(this.login.value).subscribe((result) => {
        if (result != null) {
          this.reponsedata = result;
          localStorage.setItem('token', this.reponsedata.jwtToken);
          this.route.navigate(['']);
        }
      });
    }
  }
}
